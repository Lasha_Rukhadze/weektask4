package com.example.weektask4

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.weektask4.databinding.ItemLayoutBinding

class RecyclerAdapter (private val buttons: MutableList<Item>, private val itemsListener : ItemsInterface) : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val itemView = ItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder as ItemViewHolder
        holder.bind()
    }

    override fun getItemCount(): Int {
        return buttons.size
    }

    inner class ItemViewHolder(private val binding : ItemLayoutBinding) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        private lateinit var model: Item
        fun bind() {
            model = buttons[adapterPosition]
            binding.playerButton.setImageResource(model.picture)
            binding.root.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            itemsListener.userItemOnClick(adapterPosition)
        }
    }
}