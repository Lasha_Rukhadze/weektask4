package com.example.weektask4

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridLayout
import android.widget.ImageButton
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.example.weektask4.databinding.Fragment2Binding

class Fragment2 : Fragment() {

    lateinit var binding: Fragment2Binding
    private var buttons = mutableListOf<Item>()
    lateinit var adapter: RecyclerAdapter
    private var player : Int = 0
    lateinit private var array : Array<Int>

    private val args : Fragment2Args by navArgs()
    val size : Int = args.gridSize
        val button : ImageButton = requireActivity().findViewById(R.id.playerButton)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = Fragment2Binding.inflate(inflater, container, false)

        setData(size)
        initializer()

        return binding.root
    }

    private fun play(position : Int){
        if (player % 2 ==0){
            buttons[position].picture = R.drawable.ic_x
            button.visibility = View.VISIBLE
        }
        else {
            buttons[position].picture = R.drawable.ic_zero
            button.visibility = View.VISIBLE
        }
    }
    private fun initializer(){
        adapter = RecyclerAdapter(buttons, object : ItemsInterface{
            override fun userItemOnClick(position: Int) {
                play(position)
            }
        })
        binding.recycler.layoutManager = GridLayoutManager(activity, size)
        binding.recycler.adapter = adapter
    }

    private fun setData(size : Int){
        for(i in 1..size+1){
            buttons.add(Item(R.drawable.ic_x))
        }
    }

}