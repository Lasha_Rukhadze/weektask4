package com.example.weektask4

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.weektask4.databinding.Fragment1Binding
import java.lang.Integer.parseInt

class Fragment1 : Fragment() {

    lateinit var binding: Fragment1Binding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = Fragment1Binding.inflate(inflater, container, false)
        binding.start.setOnClickListener {
            openFragment2()
        }
        // Inflate the layout for this fragment
        return binding.root
    }

    private fun openFragment2(){
        val number : Int = parseInt(binding.gridSize.text.toString())
        val action = Fragment1Directions.transition(number)
        this.findNavController().navigate(action)
    }

}